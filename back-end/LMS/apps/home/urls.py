from django.urls import path, re_path
from .views import courses, departments, category, lesson

urlpatterns = [
    # The home page
    path("", courses.index, name="home"),
    # Path to Course
    path("courses/", courses.all_courses, name="all_courses"),
    path("courses/create", courses.create_or_edit_course, name="create_courses"),
    path("courses/<int:pk>", courses.course_details, name="course_details"),
    path("courses/edit/<int:pk>", courses.create_or_edit_course, name="edit_course"),
    path("courses/delete/<int:pk>/", courses.delete_course, name="delete_course"),
    path("courses/lessons/create_lesson_course/", courses.create_lesson_in_course, name="create_lesson_in _course"),
    # Path to Department
    path("departments/", departments.all_department, name="all_departments"),
    path("departments/create", departments.create_or_edit_department, name="create_department"),
    path("departments/<int:pk>", departments.department_details, name="department_details"),
    path("departments/edit/<int:pk>", departments.create_or_edit_department, name="edit_department"),
    path("departments/delete/<int:pk>/", departments.delete_department, name="delete_department"),
    # Path to categories
    path("categorys/", category.all_category, name="all_category"),
    path("categorys/create", category.create_or_edit_category, name="create_category"),
    path("categorys/<int:pk>", category.category_details, name="category_details"),
    path("categorys/edit/<int:pk>", category.create_or_edit_category, name="edit_category"),
    path("categorys/delete/<int:pk>/", category.delete_category, name="delete_category"),
    
    # Path to lessons
    path("lessons/", lesson.all_lesson, name="all_lesson"),
    path("lessons/create", lesson.create_or_edit_lesson, name="create_lesson"),
    path("lessons/<int:pk>", lesson.lesson_details, name="lesson_details"),
    path("lessons/edit/<int:pk>", lesson.create_or_edit_lesson, name="edit_lesson"),
    path("lessons/delete/<int:pk>/", lesson.delete_lesson, name="delete_lesson"),
    # re_path(r'^.*\.*', courses.pages, name='pages'),
]
