from django.shortcuts import render, redirect, get_object_or_404
from django.forms import ModelForm
from django.core.paginator import Paginator, PageNotAnInteger
from django.http import HttpResponse
from django.template import loader


def list_model(
    request,
    model=None,
    page_size=3,
    segment=None,
    template_name=None,
    form_detail_url=None,
    specific_fields=[],
):
    field_names = get_specific_model_fields(model, specific_fields)
    title_array = [field.upper().replace('_', ' ') for field in field_names]
    print(title_array)
    instances = model.objects.all().order_by("-id")
    # course = Course.objects.filter(status='XEM ĐƯỢC').order_by('-id')
    page = request.GET.get("page", 1)
    paginator = Paginator(instances, page_size)
    try:
        instance = paginator.page(page)
    except PageNotAnInteger:
        instance = paginator.page(1)

    context = {
        "segment": segment,
        "instances": instances,
        "instance_paging": instance,
        "form_detail_url": form_detail_url,
        "fields": specific_fields,
        "titles": title_array
    }
    if not template_name:
        html_template = loader.get_template("courses-app/list-generic.html")
    else:
        html_template = loader.get_template(template_name)
    return HttpResponse(html_template.render(context, request))


def model_details(
    request,
    pk=None,
    model=None,
    many2one_model=[],
    form_class=None,
    template_name=None,
    segment=None,
):
    objects = model.objects.all()
    selected_object = model.objects.get(pk=pk)
    form = form_class(instance=selected_object)
    
    related_objects = {}
    
    if many2one_model:
        for related_model in many2one_model:
            related_name = related_model.__name__.lower() + "_set"
            related_objects[related_name] = getattr(selected_object, related_name).all() 
            #name will be like ***many2one_model_set****
            
    context = {
        "form_type": "detail-form",
        "segment": segment,
        segment: objects,
        "form": form,
        segment[:-1]: selected_object,
        **related_objects
    }
    # print(context)
    html_template = loader.get_template(template_name)
    return HttpResponse(html_template.render(context, request))


def create_or_edit_model(
    request,
    pk=None,
    model=None,
    form_class=None,
    template_name=None,
    redirect_detail_name=None,
    redirect_list_name=None,
    segment=None,
):
    context = {
        "segment": segment,
    }
    instance = get_object_or_404(model, pk=pk) if pk else None

    if request.method == "POST":
        context["form_type"] = "detail-form"
        form = form_class(request.POST, instance=instance)
        if form.is_valid():
            instance = form.save()
            return (
                redirect(redirect_detail_name, pk=instance.pk)
                if pk
                else redirect(redirect_list_name)
            )
    else:
        form = form_class(instance=instance)
        context["form_type"] = "create-form"

    context["form"] = form
    return render(request, template_name, context)


def delete_model(request, pk=None, model=None, redirect_to_name=None):
    instance = get_object_or_404(model, pk=pk)
    instance.delete()
    return redirect(redirect_to_name)


def get_model_field_names(model):
    return [field.name for field in model._meta.get_fields()]


def get_specific_model_fields(model, field_names):
    fields = [field for field in model._meta.get_fields() if field.name in field_names]
    ordered_fields = sorted(fields, key=lambda x: field_names.index(x.name))
    return [field.name for field in ordered_fields]
