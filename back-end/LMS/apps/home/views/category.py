from django import template
from django.shortcuts import redirect, render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from courses.models import Categories
from courses.forms import CategoryForm
from .baseView import *


@login_required(login_url="/login/")
def all_category(request):
    return list_model(
        request,
        model=Categories,
        specific_fields = ['name'],
        template_name=None,
        form_detail_url = "category_details",
        segment="categorys", #well just to say it's active sidebar :v
        page_size=9
    )


# form list of courses
def category_details(request, pk):
    return model_details(
        request,
        pk=pk,
        model=Categories,
        form_class=CategoryForm,
        template_name="courses-app/forms/form-category.html",
        segment="categorys"
    )

# lấy thêm tự viết hàm base tránh trùng lặp ,mong là hợp lý
def create_or_edit_category(request, pk=None):
    return create_or_edit_model(
        request,
        pk=pk,
        model=Categories,
        form_class=CategoryForm,
        template_name="courses-app/forms/form-category.html",
        redirect_detail_name="category_details",
        redirect_list_name="all_category",
        segment="categorys",
    )


def delete_category(request, pk=None):
    return delete_model(request, pk=pk, model=Categories, redirect_to_name="all_category")
