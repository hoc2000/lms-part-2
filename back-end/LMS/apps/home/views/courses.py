from django import template
from django.shortcuts import redirect, render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.template import loader
from django.urls import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from courses.models import Course, Lesson
from courses.forms import CourseForm
from .baseView import *


@login_required(login_url="/login/")
def index(request):
    context = {"segment": "index"}

    html_template = loader.get_template("home/index.html")
    return HttpResponse(html_template.render(context, request))


@login_required(login_url="/login/")
def pages(request):
    context = {}
    # All resource paths end in .html.
    # Pick out the html file name from the url. And load that template.
    try:

        load_template = request.path.split("/")[-1]

        if load_template == "admin":
            return HttpResponseRedirect(reverse("admin:index"))
        context["segment"] = load_template

        html_template = loader.get_template("home/" + load_template)
        return HttpResponse(html_template.render(context, request))

    except template.TemplateDoesNotExist:

        html_template = loader.get_template("home/page-404.html")
        return HttpResponse(html_template.render(context, request))

    except:
        html_template = loader.get_template("home/page-500.html")
        return HttpResponse(html_template.render(context, request))


@login_required(login_url="/login/")
def all_courses(request):
    return list_model(
        request,
        model=Course,
        specific_fields = ['title', 'status', 'author', 'price', 'created_at'],
        template_name=None,
        form_detail_url = "course_details",
        segment="courses", #well just to say it's active sidebar :v
        page_size=9
    )


# form list of courses
def course_details(request, pk):
    return model_details(
        request,
        pk=pk,
        model=Course,
        many2one_model = [Lesson],
        form_class=CourseForm,
        template_name="courses-app/forms/form-course.html",
        segment="courses"
    )

# lấy thêm tự viết hàm base tránh trùng lặp ,mong là hợp lý
def create_or_edit_course(request, pk=None):
    return create_or_edit_model(
        request,
        pk=pk,
        model=Course,
        form_class=CourseForm,
        template_name="courses-app/forms/form-course.html",
        redirect_detail_name="course_details",
        redirect_list_name="all_courses",
        segment="courses",
    )
    
def create_lesson_in_course(request):
    if request.method == "POST":
        lesson_name = request.POST.get("name")
        course = Course.objects.get(pk=request.POST.get("courseId"))
        
        lesson = Lesson(course=course, name=lesson_name)
        lesson.save()
        return JsonResponse({"id": lesson.id, "name": lesson.name})
    return JsonResponse({"error": "Invalid request"}, status=400)




def delete_course(request, pk=None):
    return delete_model(request, pk=pk, model=Course, redirect_to_name="all_courses")
