from django import template
from django.shortcuts import redirect, render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from courses.models import Department
from courses.forms import DepartmentForm
from .baseView import *


@login_required(login_url="/login/")
def all_department(request):
    return list_model(
        request,
        model=Department,
        specific_fields = ['name', 'created_at'],
        template_name=None,
        form_detail_url = "department_details",
        segment="departments",
        page_size=9,
    )


def department_details(request, pk):
    return model_details(
        request,
        pk=pk,
        model=Department,
        form_class=DepartmentForm,
        template_name="courses-app/forms/form-department.html",
        segment="departments",
    )


def create_or_edit_department(request, pk=None):
    return create_or_edit_model(
        request,
        pk=pk,
        model=Department,
        form_class=DepartmentForm,
        template_name="courses-app/forms/form-department.html",
        redirect_detail_name="department_details",
        redirect_list_name="all_departments",
        segment="departments",
    )

def delete_department(request, pk=None):
    return delete_model(request, pk=pk, model=Department, redirect_to_name="all_departments")