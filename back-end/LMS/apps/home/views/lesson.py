from django import template
from django.shortcuts import redirect, render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.template import loader
from django.urls import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from courses.models import Lesson
from courses.forms import LessonForm
from .baseView import *


@login_required(login_url="/login/")
def all_lesson(request):
    return list_model(
        request,
        model=Lesson,
        specific_fields=["name", "course"],
        template_name=None,
        form_detail_url="lesson_details",
        segment="lessons",  # well just to say it's active sidebar :v
        page_size=9,
    )


# form list of courses
def lesson_details(request, pk):
    return model_details(
        request,
        pk=pk,
        model=Lesson,
        form_class=LessonForm,
        template_name="courses-app/forms/form-lesson.html",
        segment="lessons",
    )


# lấy thêm tự viết hàm base tránh trùng lặp ,mong là hợp lý
def create_or_edit_lesson(request, pk=None):
    return create_or_edit_model(
        request,
        pk=pk,
        model=Lesson,
        form_class=LessonForm,
        template_name="courses-app/forms/form-lesson.html",
        redirect_detail_name="lesson_details",
        redirect_list_name="all_lesson",
        segment="lessons",
    )


def delete_lesson(request, pk=None):
    return delete_model(request, pk=pk, model=Lesson, redirect_to_name="all_lesson")
