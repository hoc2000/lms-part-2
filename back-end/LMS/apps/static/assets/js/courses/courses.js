$(document).ready(function() {
    const $card = $('.card-scroll-follow');

    $(window).on('scroll', function() {
        const scrollTop = $(window).scrollTop();
        const cardTop = $card.offset().top;
        const cardHeight = $card.height();

        if (scrollTop + cardHeight > cardTop) {
            $card.css('top', scrollTop + 'px');
        }
    });


    $('#courseButton').click(function() {
        console.log("hello");
        $('#courseForm').submit();
    });
    

    const initialData = {}; //old data
    $('#courseForm').find(':input').each(function() {
        initialData[this.id] = $(this).val();
    });

    console.log(initialData);
    // Function to check if form data has changed
    function hasFormChanged() {
        let isChanged = false;
        $('#courseForm').find(':input').each(function() {
            if (initialData[this.id] !== $(this).val()) {
                isChanged = true;
                return false; // Exit each loop early if any change is detected
            }
        });
        return isChanged;
    }

    // Event listener for input changes
    $('#courseForm').on('input', function () {
        const isChanged = hasFormChanged();
        $('#courseButton').toggle(isChanged);
    });


    function deleteLesson(lessonId) {
        console.log('{% url "delete_lesson" 0 %}');
        // $.ajax({
        //     url: '{% url "delete_lesson" 0 %}'.replace('0', lessonId),
        //     type: 'POST',
        //     data: {
        //         csrfmiddlewaretoken: '{{ csrf_token }}'
        //     },
        //     success: function(response) {
        //         if (response.success) {
        //             $('#lesson-' + lessonId).remove();
        //         } else {
        //             alert(response.error);
        //         }
        //     },
        //     error: function(xhr, status, error) {
        //         console.error(error);
        //         alert('An error occurred. Please try again.');
        //     }
        // });
    }
});


function redirectTo(url) {
    window.location.href = url;
}
