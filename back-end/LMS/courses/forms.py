from django.forms import ModelForm
from .models import Course, Department, Categories, Lesson, Video

class CourseForm(ModelForm):
        class Meta:
            model = Course
            fields = ['id','title','author','category','price','discount','language','description','status','language','date_start']
            
class DepartmentForm(ModelForm):
        class Meta:
            model = Department
            fields = '__all__'

class CategoryForm(ModelForm):
    class Meta:
        model = Categories
        fields = '__all__'
        
class LessonForm(ModelForm):
    class Meta:
        model = Lesson
        fields = '__all__'
        
class VideoForm(ModelForm):
    class Meta:
        model = Video
        fields = '__all__'