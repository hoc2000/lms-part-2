from django.urls import path, include
from . import views

urlpatterns = [
    path('courses/', views.course_list, name="get-courses-list"),
    path('courses/details/<int:pk>/', views.course_detail, name="course-detail"),
    # path('wishes/delete/<int:pk>/', views.deleteWish, name="delete-wishes")
]
