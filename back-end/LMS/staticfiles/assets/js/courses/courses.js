function getCSRFToken() {
    const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    return csrfToken;
}

$(document).ready(function() {
    const csrftoken = getCSRFToken();
    console.log(csrftoken);
    $.ajaxSetup({
        headers: {
            'X-CSRFToken': csrftoken
        }
    });

    const $card = $('.card-scroll-follow');

    $(window).on('scroll', function() {
        console.log();
        const scrollTop = $(window).scrollTop();
        const cardTop = $card.offset().top;
        const cardHeight = $card.height();

        if (scrollTop + cardHeight > cardTop) {
            $card.css('top', (scrollTop) + 'px');
        }
    });


    $('#courseButton').click(function() {
        console.log("hello");
        $('#courseForm').submit();
    });
    

    const initialData = {}; //old data
    $('#courseForm').find(':input').each(function() {
        initialData[this.id] = $(this).val();
    });

    console.log(initialData);
    // Function to check if form data has changed
    function hasFormChanged() {
        let isChanged = false;
        $('#courseForm').find(':input').each(function() {
            if (initialData[this.id] !== $(this).val()) {
                isChanged = true;
                return false; // Exit each loop early if any change is detected
            }
        });
        return isChanged;
    }

    // Event listener for input changes
    $('#courseForm').on('input', function () {
        const isChanged = hasFormChanged();
        $('#courseButton').toggle(isChanged);
    });

    function handleRowClick() {
        var redirectUrl = $(this).attr('url');
        window.location.href = redirectUrl;
    }
    $('tr.data_row_table').click(handleRowClick);


    $(document).on('click', '.add_lesson', function() {
        console.log("herllo");
        $('tr#input_add_lesson_row').show()
        $('tr#add_lesson_row').hide()
    });


});


function deleteLesson(lessonId) {
    var urldelete = $(`#deleteLesson-${lessonId}`).attr('url');
    // console.log(urldelete);
    $.ajax({
        url: urldelete,
        type: 'POST',
        success: function(response) {
            $('#lesson-' + lessonId).remove();
            // if (response.success) {
            //     $('#lesson-' + lessonId).remove();
            // } else {
            //     alert(response.error);
            // }
        },
        error: function(xhr, status, error) {
            console.error(error);
            alert('An error occurred. Please try again.');
        }
    });
}

function toggleInput(){
    $('tr#add_lesson_row').show()
    $('tr#input_add_lesson_row').hide()
}

function addLesson(courseId) {
    var $tr = $(this).closest('tr');
    const csrftoken = getCSRFToken();
    lesson_name = $('.lesson_input').val()
    // console.log(urldelete);
    if(lesson_name){
        $.ajax({
            url: 'lessons/create_lesson_course/',
            type: 'POST',
            data: {
                'courseId': courseId,
                'name': lesson_name,
                'csrfmiddlewaretoken': csrftoken,
            },
            success: function(response) {
                if (response.error) {
                    alert(response.error);
                } else {
                    var lessonId = response.id;
                    var lessonName = response.name;
    
                    var updatedContent = `
                    <tr id="lesson-${lessonId}" class="child_data_table" data-id="${lessonId}">
                        <td class="w-100 budget py-3" style="font-size: 15px;">
                            ${lessonName}
                        </td>
                        <td
                            <a id="deleteLesson-${lessonId}" class="float-right" onclick="deleteLesson(${lessonId})" url="/lessons/delete/${lessonId}/">    
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                    `;
                    toggleInput()
                    $('tr#add_lesson_row').before(updatedContent);
                    $('.lesson_input').val('')
                }
            },
            error: function(xhr, status, error) {
                console.error('Error creating lesson:', error);
            }
        });
    }else{
        alert("Please type your lesson name input")
    }
    
}



