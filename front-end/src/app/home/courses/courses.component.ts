import { Component } from '@angular/core';
import { CourseItemComponent } from "./course-item/course-item.component";

@Component({
  selector: 'app-courses',
  standalone: true,
  imports: [CourseItemComponent],
  templateUrl: './courses.component.html',
  styleUrl: './courses.component.scss'
})
export class CoursesComponent {
  items = [1,2,3,4,5];

}
