import { Component } from '@angular/core';
import { IntroductionComponent } from './introduction/introduction.component';
import { CoursesComponent } from './courses/courses.component';
import { TrendingComponent } from "./trending/trending.component";

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [IntroductionComponent, CoursesComponent, TrendingComponent],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent {

}
