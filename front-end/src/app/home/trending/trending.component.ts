import { Component } from '@angular/core';
import { CourseItemComponent } from "../courses/course-item/course-item.component";

@Component({
  selector: 'app-trending',
  standalone: true,
  imports: [CourseItemComponent],
  templateUrl: './trending.component.html',
  styleUrl: './trending.component.scss'
})
export class TrendingComponent {
  items = [1,2,3,4,5];
}
